<?php

namespace Klupp\GatewayPayment\Contracts\Payments;

use Klupp\GatewayPayment\Contracts\Controllers\CardsController;use Klupp\GatewayPayment\Contracts\Controllers\ChargesController;
use Klupp\GatewayPayment\Contracts\Controllers\CustomersController;
use Klupp\GatewayPayment\Contracts\Controllers\OrdersController;
use Klupp\GatewayPayment\Contracts\Controllers\PlansController;
use Klupp\GatewayPayment\Contracts\Controllers\SubscriptionsController;

interface Payment 
{
  public function getOrders(): OrdersController;

  public function getCharges(): ChargesController;

  public function getCustomers(): CustomersController;

  public function getSubscriptions(): SubscriptionsController;

  public function getPlans(): PlansController;

  public function getCards(): CardsController;
}