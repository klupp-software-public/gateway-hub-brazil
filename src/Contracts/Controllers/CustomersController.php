<?php

namespace Klupp\GatewayPayment\Contracts\Controllers;

interface CustomersController
{
  

    /**
     * @param array $body_params
     * @return array|null
     */
    public function createCustomer(array $body_params);

    /**
     * @param array $params
     * @return array
     */
    public function listCustomers(array $params = []): array;

    /**
     * @param string $id
     * @return array|null
     */
    public function getCustomer(string $id): ?array;

    /**
     * @param string $id
     * @param array $body_params
     * @return array|null
     */
    public function updateCustomer(string $id, array $body_params);
}