<?php

namespace Klupp\GatewayPayment\Contracts\Controllers;

interface OrdersController 
{
  


    /**
     * @param array $body_params
     * @return array|null
     */
    public function createOrder(array $body_params);

    /**
     * @param array $params
     * @return array
     */
    public function listOrders(array $params = []): array;

    /**
     * @param string $id
     * @return array|null
     */
    public function getOrder(string $id): ?array;

    /**
     * @param string $id
     * @param array $body_params
     * @return array|null
     */
    public function updateOrder(string $id, array $body_params);
}