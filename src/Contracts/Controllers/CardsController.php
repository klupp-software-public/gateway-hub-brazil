<?php

namespace Klupp\GatewayPayment\Contracts\Controllers;

interface CardsController
{
    /**
     * @param string $customer_id
     * @param array $body_params
     * @return array|null
     */
    public function createCard(string $customer_id, array $body_params);

    /**
     * @param string $customer_id
     * @return array
     */
    public function listCards(string $customer_id, array $params = []): array;

    /**
     * @param string $customer_id
     * @param  string $card_id
     * @return array|null
     */
    public function getCard(string $customer_id, string $card_id): ?array;

    /**
     * @param string $customer_id
     * @param string $card_id
     * @return array|null
     */
    public function updateCard(string $customer_id, string $card_id, array $body_params);

    /**
     * @param string $customer_id
     * @param string $card_id
     * @return array|null
     */
    public function deleteCard(string $customer_id, string $card_id);
}