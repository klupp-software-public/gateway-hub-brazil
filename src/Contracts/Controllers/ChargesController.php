<?php

namespace Klupp\GatewayPayment\Contracts\Controllers;

interface ChargesController 
{
  


    /**
     * @param array $body_params
     * @return array|null
     */
    public function createCharge(array $body_params);


    /**
     * @param array $params
     * @return array
     */
    public function listCharges(array $params = []): array;

    /**
     * @param string $id
     * @return array|null
     */
    public function getCharge(string $id): ?array;

    /**
     * @param string $id
     * @param array $body_params
     * @return array|null
     */
    public function updateCharge(string $id, array $body_params);

    /**
     * @param string $id
     * @param array $options
     * @return array|null
     */
    public function cancelCharge(string $id, array $options = []);
}