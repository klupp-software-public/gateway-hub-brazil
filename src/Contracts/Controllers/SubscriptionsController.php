<?php

namespace Klupp\GatewayPayment\Contracts\Controllers;

/**
 * @link https://www.mercadopago.com.br/developers/pt/reference/subscriptions/_preapproval/post
 * 
 * Subscriptions
 * 
 * @link https://www.mercadopago.com.br/developers/pt/reference/subscriptions/_authorized_payments_id/get
 * 
 * Invoices
 */
interface SubscriptionsController 
{
  

    /**
     * 
     * @param array $body_params
     * @return array|null
     */
    public function createSubscription(array $body_params);

    /**
     *
     * @param array $params
     * @return array
     */
    public function listSubscriptions(array $params = []): array;

    /**
     *
     * @param string $id
     * @return array|null
     */
    public function getSubscription(string $id): ?array;


    /**
     *
     * @param string $id
     * @param array $body_params
     * @return array|null
     */
    public function updateSubscription(string $id, array $body_params);

    /**
     *
     * @param string $id
     * @return array|null
     */
    public function cancelSubscription(string $id);
}