<?php

namespace Klupp\GatewayPayment\Contracts\Controllers;

interface PlansController 
{
  


    /**
     * @param array $body_params
     * @return array|null
     */
    public function createPlan(array $body_params);

    /**
     * Undocumented function
     *
     * @param array $params
     * @return array
     */
    public function listPlans(array $params = []): array;

    /**
     * @param string $id
     * @return array|null
     */
    public function getPlan(string $id): ?array;


    /**
     * @param string $id
     * @param array $body_params
     * @return array|null
     */
    public function updatePlan(string $id, array $body_params);

    /**
     * @param string $id
     * @return array|null
     */
    public function deletePlan(string $id);
}