<?php

namespace Klupp\GatewayPayment\Helpers;

use Psr\Http\Message\ResponseInterface;

class HttpHelper
{
    /**
     *  Elimina os items pela chave que não estão no array
     * @param  $keys
     * @param array $values
     * @return array
     */
    public static function sanitize(array $keys, array $values): array
    {
        $new_values = [];
        
        foreach($keys as $key) {
            if(array_key_exists($key, $values)) {
                $new_values[$key] = $values[$key];
            }
        } 

        return $new_values;
    }

    /**
     * Converte a estrutura JSON em array
     *
     * @param string|ResponseInterface $value
     * @return null|array
     */
    public static function toArray($value): ?array
    {
        $options = [true, 1000, JSON_OBJECT_AS_ARRAY | JSON_PARTIAL_OUTPUT_ON_ERROR];

        if($value instanceof ResponseInterface)
            $value = $value->getBody()->getContents();

         return !empty($value) ? json_decode($value, ...$options) : [];
    }

    public static function getParams(array $params): string
    {
        $query_params = '';

        if (count($params) > 0)
            $query_params = '?' . http_build_query($params);

        return $query_params;
    }
}