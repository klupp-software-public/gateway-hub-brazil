<?php

namespace Klupp\GatewayPayment\Helpers;

use Psr\Http\Message\ResponseInterface;

class PaymentException extends \Exception
{
    private ?ResponseInterface $response = null;
    private array $headers = [];
    private int $status_code = 0;

    /**
     *
     * @param \Throwable|ResponseInterface $exception
     * @param string|null $message
     * @param integer $code
     */
    public function __construct(
        $exception,
        ?string $message = null,
        int $code = 0
    ) {
        if ($exception instanceof ResponseInterface) {
            $this->headers = $exception->getHeaders();
            $this->status_code = $exception->getStatusCode();
            $this->response = $exception;

            $message = $message ?? $exception->getBody()->getContents();
            $exception = $this;
        }

        parent::__construct($message, $code, $exception);
    }

    public function getStatusCode(): int
    {
        return $this->status_code;
    }

    public function getHeaders(): array
    {
        return $this->headers;
    }

    public function getResponse(): ?ResponseInterface
    {
        return $this->response;
    }

    public function getFromJson()
    {
        return HttpHelper::toArray(
            $this
                ->response
                ->getBody()
                ->getContents()
        );
    }
}