<?php
namespace Klupp\GatewayPayment\Helpers;

trait Singleton
{
    private static ?self $instance = null;

    private function __construct() {}

    /**
     * returns a singleton instance
     *
     * @return static
     */
    public static function getInstance()
    {
        if(empty(static::$instance)) {
            static::$instance = new static();
        }

        return static::$instance;
    }
}