<?php

namespace Klupp\GatewayPayment\MercadoPago\Controllers;


use Klupp\GatewayPayment\Http;
use Klupp\GatewayPayment\Helpers\Singleton;
use Klupp\GatewayPayment\Helpers\HttpHelper;
use Klupp\GatewayPayment\Contracts\Controllers\ChargesController;

use Psr\Http\Message\ResponseInterface;

class MercadoPagoChargesController implements ChargesController
{
    use Singleton;

    private string $prefix = 'v1/payments';

    public function createCharge(array $body_params)
    {
        /**
         * @var ResponseInterface $response
         */
        $response = Http::post(
            $this->prefix,
            null,
            json_encode($body_params)
        );

        return HttpHelper::toArray($response);
    }

    public function listCharges(array $params = []): array
    {
        $params = HttpHelper::getParams($params);

        /**
         * @var ResponseInterface $response
         */
        $response = Http::get(
            "{$this->prefix}/search{$params}",
        );


        return HttpHelper::toArray($response);
    }

    public function getCharge(string $id): array
    {
        /**
         * @var ResponseInterface $response
         */
        $response = Http::get(
            "{$this->prefix}/$id"
        );

        return HttpHelper::toArray($response);
    }

    public function updateCharge(string $id, array $body_params)
    {
        /**
         * @var ResponseInterface $response
         */
        $response = Http::put(
            "{$this->prefix}/$id",
            null,
            json_encode($body_params)
        );

        return HttpHelper::toArray($response);
    }

    public function cancelCharge(string $id, array $options = [])
    {
        $options = HttpHelper::sanitize(['body'], $options);

        /**
         * @var ResponseInterface $response
         */
        $response = Http::put(
            "{$this->prefix}/$id",
            null,
            isset($options['body']) ? json_encode($options['body']) : null
        );

        return HttpHelper::toArray($response);
    }

    public function getPayments(): array
    {
        /**
         * @var ResponseInterface $response
         */
        $response = Http::get('payment_methods');

        return HttpHelper::toArray($response);
    }

    public function createRefund(string $id, array $body_params, ?string $xIdempotencyKey = null)
    {
        /**
         * @var ResponseInterface $response
         */
        $response = Http::post(
            "{$this->prefix}/$id/refunds",
            $xIdempotencyKey ? ['X-Idempotency-Key' => $xIdempotencyKey] : null,
            $body_params
        );

        return HttpHelper::toArray($response);
    }

    public function listRefund(string $id): array
    {
        /**
         * @var ResponseInterface $response
         */
        $response = Http::get("{$this->prefix}/$id/refunds");

        return HttpHelper::toArray($response);
    }


    public function getRefund(string $id, string $refund_id)
    {
        /**
         * @var ResponseInterface $response
         */
        $response = Http::get("{$this->prefix}/$id/refunds/$refund_id");

        return HttpHelper::toArray($response);
    }
}