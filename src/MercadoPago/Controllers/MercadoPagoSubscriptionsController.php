<?php

namespace Klupp\GatewayPayment\MercadoPago\Controllers;

use Klupp\GatewayPayment\Contracts\Controllers\SubscriptionsController;
use Klupp\GatewayPayment\Helpers\Singleton;
use Klupp\GatewayPayment\Http;
use Klupp\GatewayPayment\Helpers\HttpHelper;

use Psr\Http\Message\ResponseInterface;

class MercadoPagoSubscriptionsController implements SubscriptionsController
{
    use Singleton;

    private string $prefix = 'preapproval';
    private string $prefix_invoice = 'authorized_payments';

    public function createSubscription(array $body_params)
    {
        /**
         * @var ResponseInterface $response
         */
        $response = Http::post(
            $this->prefix,
            null,
            json_encode($body_params)
        );

        return HttpHelper::toArray($response);
    }

    public function listSubscriptions(array $params = []): array
    {
        $params = HttpHelper::getParams($params);

        /**
         * @var ResponseInterface $response
         */
        $response = Http::get(
            "{$this->prefix}/search{$params}"
        );

        return HttpHelper::toArray($response);
    }

    public function getSubscription(string $id): array
    {
        /**
         * @var ResponseInterface $response
         */
        $response = Http::get(
            "{$this->prefix}/$id"
        );

        return HttpHelper::toArray($response);
    }

    public function updateSubscription(string $id, array $body_params)
    {
        /**
         * @var ResponseInterface $response
         */
        $response = Http::put(
            "{$this->prefix}/$id",
            null,
            json_encode($body_params)
        );

        return HttpHelper::toArray($response);
    }

    public function cancelSubscription(string $id)
    {
        throw new \LogicException('Method not contains implementations');
    }

    public function exportSubscriptions(array $params = [])
    {
        $params = HttpHelper::getParams($params);

        /**
         * @var ResponseInterface $response
         */
        $response = Http::get(
            "{$this->prefix}{$params}"
        );

        return HttpHelper::toArray($response);
    }

    /**
     * Alguns parâmetros de pesquisas são obrigatórios
     * 
     * @link https://www.mercadopago.com.br/developers/pt/reference/subscriptions/_authorized_payments_search/get
     *
     * @param array $params
     * @return array
     */
    public function listInvoices(array $params): array
    {
        $params = HttpHelper::getParams($params);

        /**
         * @var ResponseInterface $response
         */

        $response = Http::get(
            "{$this->prefix_invoice}/search{$params}"
        );

        return HttpHelper::toArray($response);
    }

    public function getInvoice(string $id): ?array
    {
        /**
         * @var ResponseInterface $response
         */
        $response = Http::get(
            "{$this->prefix_invoice}/{$id}"
        );

        return HttpHelper::toArray($response);
    }
}