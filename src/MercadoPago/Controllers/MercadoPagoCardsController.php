<?php

namespace Klupp\GatewayPayment\MercadoPago\Controllers;

use Klupp\GatewayPayment\Http;
use Klupp\GatewayPayment\Helpers\HttpHelper;
use Klupp\GatewayPayment\Helpers\Singleton;
use Klupp\GatewayPayment\Contracts\Controllers\CardsController;

class MercadoPagoCardsController implements CardsController
{
    use Singleton;
    
    private string $prefix = 'v1/customers/{customer_id}/cards';

    public function createCard(string $customer_id, array $body_params)
    {
        $this->replacePrefix($customer_id);

        /**
         * @var ResponseInterface $response
         */
        $response = Http::post(
            $this->prefix,
            null,
            json_encode($body_params)
        );

        return HttpHelper::toArray($response);
    }

    public function listCards(string $customer_id, array $params = []): array
    {
        $this->replacePrefix($customer_id);

        $params = HttpHelper::getParams($params);

        /**
         * @var ResponseInterface $response
         */
        $response = Http::get(
            "{$this->prefix}{$params}"
        );

        return HttpHelper::toArray($response);
    }

    public function getCard(string $customer_id, string $card_id): array
    {
        $this->replacePrefix($customer_id);

        /**
         * @var ResponseInterface $response
         */
        $response = Http::get(
            "{$this->prefix}/$card_id"
        );

        return HttpHelper::toArray($response);
    }

    public function updateCard(string $customer_id, string $card_id, array $body_params)
    {
        $this->replacePrefix($customer_id);

        /**
         * @var ResponseInterface $response
         */
        $response = Http::put(
            "{$this->prefix}/$card_id",
            null,
            json_encode($body_params)
        );

        return HttpHelper::toArray($response);
    }

    public function deleteCard(string $customer_id, string $card_id)
    {
        $this->replacePrefix($customer_id);

        /**
         * @var ResponseInterface $response
         */
        $response = Http::delete(
            "{$this->prefix}/$card_id"
        );

        return HttpHelper::toArray($response);
    }

    private function replacePrefix(string $customer_id)
    {
        $this->prefix = preg_replace('/{customer_id}/', $customer_id, $this->prefix);
    }
}