<?php

namespace Klupp\GatewayPayment\MercadoPago\Controllers;

use Klupp\GatewayPayment\Http;
use Klupp\GatewayPayment\Helpers\Singleton;
use Klupp\GatewayPayment\Helpers\HttpHelper;
use Klupp\GatewayPayment\Contracts\Controllers\OrdersController;

class MercadoPagoOrdersController implements OrdersController
{
    use Singleton;

    protected string $prefix = 'merchant_orders';

    public function createOrder(array $body_params)
    {
        /**
         * @var ResponseInterface $response
         */
        $response = Http::post(
            $this->prefix,
            null,
            json_encode($body_params)
        );

        return HttpHelper::toArray($response);
    }

    public function listOrders(array $params = []): array
    {
        $params = HttpHelper::getParams($params);

        /**
         * @var ResponseInterface $response
         */
        $response = Http::get(
            "{$this->prefix}/search{$params}"
        );

        return HttpHelper::toArray($response);
    }

    public function getOrder(string $id): ?array
    {


        /**
         * @var ResponseInterface $response
         */
        $response = Http::get(
            "{$this->prefix}/$id"
        );

        return HttpHelper::toArray($response);
    }

    public function updateOrder(string $id, array $body_params)
    {
        /**
         * @var ResponseInterface $response
         */
        $response = Http::put(
            "{$this->prefix}/$id",
            null,
            json_encode($body_params)
        );

        return HttpHelper::toArray($response);
    }
}