<?php

namespace Klupp\GatewayPayment\MercadoPago\Controllers;


use Klupp\GatewayPayment\Contracts\Controllers\CustomersController;
use Klupp\GatewayPayment\Helpers\Singleton;
use Klupp\GatewayPayment\Helpers\HttpHelper;
use Klupp\GatewayPayment\Http;
use Psr\Http\Message\ResponseInterface;

class MercadoPagoCustomersController implements CustomersController
{
    use Singleton;

    private string $prefix = 'v1/customers';

    public function createCustomer(array $body_params)
    {
        /**
         * @var ResponseInterface $response
         */
        $response = Http::post(
            $this->prefix,
            null,
            json_encode($body_params)
        );

        return HttpHelper::toArray($response);
    }

    public function listCustomers(array $params = []): array
    {
        $params = HttpHelper::getParams($params);


        /**
         * @var ResponseInterface $response
         */
        $response = Http::get(
            "{$this->prefix}/search{$params}",
        );


        return HttpHelper::toArray($response);
    }

    public function getCustomer(string $id): ?array
    {
        /**
         * @var ResponseInterface $response
         */
        $response = Http::get(
            "{$this->prefix}/$id",
        );

        return  HttpHelper::toArray($response) ?? null;
    }

    public function updateCustomer(string $id, array $body_params)
    {
        /**
         * @var ResponseInterface $response
         */
        $response = Http::put(
            "{$this->prefix}/$id",
            null,
            json_encode($body_params)
        );

        return HttpHelper::toArray($response);
    }
}