<?php

namespace Klupp\GatewayPayment\MercadoPago\Controllers;

use Klupp\GatewayPayment\Http;
use Klupp\GatewayPayment\Contracts\Controllers\PlansController;
use Klupp\GatewayPayment\Helpers\Singleton;
use Klupp\GatewayPayment\Helpers\HttpHelper;

use Psr\Http\Message\ResponseInterface;

class MercadoPagoPlansController implements PlansController
{
    use Singleton;

    private string $prefix = 'preapproval_plan';

    public function createPlan(array $body_params)
    {
        /**
         * @var ResponseInterface $response
         */
        $response = Http::post(
            $this->prefix,
            null,
            json_encode($body_params)
        );

        return HttpHelper::toArray($response);
    }

    public function listPlans(array $params = []): array
    {
        $params = HttpHelper::getParams($params);

        /**
         * @var ResponseInterface $response
         */
        $response = Http::get(
            "{$this->prefix}/search{$params}"
        );

        return HttpHelper::toArray($response);   
    }

    public function getPlan(string $id): array
    {
        /**
         * @var ResponseInterface $response
         */
        $response = Http::get(
            "{$this->prefix}/$id",
        );

        return HttpHelper::toArray($response);
    }

    public function updatePlan(string $id, array $body_params)
    {
        /**
         * @var ResponseInterface $response
         */
        $response = Http::put(
            "{$this->prefix}/$id",
            null,
            json_encode($body_params)
        );

        return HttpHelper::toArray($response);
    }

    public function deletePlan(string $id)
    {
        throw new \LogicException('Method not contains implementations');
    }
}