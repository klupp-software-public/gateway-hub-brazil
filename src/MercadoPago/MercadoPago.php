<?php

namespace Klupp\GatewayPayment\MercadoPago;

use Klupp\GatewayPayment\Http;

use Klupp\GatewayPayment\Contracts\Payments\Payment;
use Klupp\GatewayPayment\Contracts\Controllers\CardsController;
use Klupp\GatewayPayment\Contracts\Controllers\ChargesController;
use Klupp\GatewayPayment\Contracts\Controllers\CustomersController;
use Klupp\GatewayPayment\Contracts\Controllers\OrdersController;
use Klupp\GatewayPayment\Contracts\Controllers\PlansController;
use Klupp\GatewayPayment\Contracts\Controllers\SubscriptionsController;

use Klupp\GatewayPayment\MercadoPago\Controllers\MercadoPagoCardsController;
use Klupp\GatewayPayment\MercadoPago\Controllers\MercadoPagoChargesController;
use Klupp\GatewayPayment\MercadoPago\Controllers\MercadoPagoCustomersController;
use Klupp\GatewayPayment\MercadoPago\Controllers\MercadoPagoOrdersController;
use Klupp\GatewayPayment\MercadoPago\Controllers\MercadoPagoPlansController;
use Klupp\GatewayPayment\MercadoPago\Controllers\MercadoPagoSubscriptionsController;

class MercadoPago implements Payment
{
    private string $base_url = 'https://api.mercadopago.com/';

    public function __construct(string $token, bool $async = false)
    {
        Http::config([
            'base_url' => $this->base_url,
            'async' => $async,
            'headers' => [
                'Authorization' => "Bearer $token",
                'Content-Type' => 'application/json; charset=utf-8',
                'Accept' => 'application/json',
                'Accept-Charset' => 'utf-8, iso-8859-1;q=1'
            ]
        ]);
    }

    public function getOrders(): OrdersController
    {
        return MercadoPagoOrdersController::getInstance();
    }

    public function getCharges(): ChargesController
    {
        return MercadoPagoChargesController::getInstance();
    }

    public function getCustomers(): CustomersController
    {
        return MercadoPagoCustomersController::getInstance();
    }

    public function getSubscriptions(): SubscriptionsController
    {
        return MercadoPagoSubscriptionsController::getInstance();
    }

    public function getPlans(): PlansController
    {
        return MercadoPagoPlansController::getInstance();
    }

    public function getCards(): CardsController
    {
        return MercadoPagoCardsController::getInstance();
    }
}