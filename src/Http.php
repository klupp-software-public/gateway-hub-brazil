<?php

namespace Klupp\GatewayPayment;

use Klupp\GatewayPayment\Helpers\HttpHelper;
use Klupp\GatewayPayment\Helpers\PaymentException;
use React\Http\Browser;
use React\Http\Message\ResponseException;
use React\Promise\PromiseInterface;
use function React\Async\await;

class Http
{
    private static ?Browser $client = null;
    private static array $options = [];

    private function __construct()
    {
    }


    public static function __callStatic($method_name, $arguments)
    {
        try {
            if (!method_exists(static::$client, $method_name)) {
                throw new \Exception("Method $method_name not found");
            }

            $options = static::$options;
            $async = $options['async'];

            unset($options['async'], $options['base_url']);

            if (array_key_exists('headers', $options)) {
                !empty($arguments[1]) ?
                    array_push($arguments[1], $options['headers'])
                    : ($arguments[1] = $options['headers']);
            } else {
                $arguments[1] = $arguments[1] ?? [];
            }

            $response = static::$client->{$method_name}(...$arguments);


            if ($response instanceof PromiseInterface && !$async) {
                return await($response);
            }

            return $response;
        } catch (\Throwable $th) {
            static::listenException($th);
        }
    }

    /**
     * Create a new http client instance
     * @param array $options
     * @return void
     */
    public static function newInstance(array $options = []): void
    {
        static::$client = null;

        $options = HttpHelper::sanitize(
            ['base_url', 'async', 'headers'],
            $options
        );

        static::$options = $options;
        static::$options['async'] = $options['async'] ?? false;

        static::setInstance();
    }


    /**
     * Configure the settings for the client instance
     *
     * @param array $options base_url, async, headers
     * @return void
     */
    public static function config(array $options = []): void
    {
        $options = HttpHelper::sanitize(
            ['base_url', 'async', 'headers'],
            $options
        );

        static::$options = $options;
        static::$options['async'] = $options['async'] ?? false;

        static::setInstance();
    }

    /**
     * Create an instance of the http client
     *
     * @return void
     */
    private static function setInstance(): void
    {
        if (empty(static::$client)) {
            static::$client = new Browser();

            !empty(static::$options['base_url']) && (static::$client = static::$client->withBase(static::$options['base_url']));
        }
    }


    private static function listenException(\Throwable $th)
    {
        throw new PaymentException(
            $th instanceof ResponseException
                ? $th->getResponse()
                : $th
        );
    }
}