## Padrões

* As classe do gateway de pagamento segue os padrões proposto em Contracts\Payments\Payment, caso você achou algum padrão entre os gateway fique a vontade para adicionar na interface ou caso achou uma divergência muito grande na interface informe sobre e tente usar o padrão **Adapter**

* O namespace tem que seguir o padrão `Klupp\GatewayPayment\$GATEWAY`

* Adicione tipagem nos parâmetro e retorno dos métodos

* Documente os métodos sobre o que ele faz ou deixe um link como referência

* Logo abaixo tem as referência de bibliotecas usada nesse biblioteca, quando estiver usando alguma biblioteca de terceiros adicione nas referências (É importante usar menos depêndencias para manter estável)

* Os teste unitários não é obrigatório você pode usar um depurador ou testar com teste unitários (Sempre teste antes de qualquer coisa).


## Referências

[React\http](https://reactphp.org/http/#client-usage)

[Codeception](https://codeception.com/docs/UnitTests)